#pragma once

#include <QMainWindow>
#include <QBasicTimer>

#include "us.h"
#include "us_rawnet_multi.h"
#include "us_time.h"

#include "JDKSAvdeccSimple.h"
#include "JDKSAvdeccNetIO.hpp"
#include "JDKSAvdeccControllerEntity.hpp"

namespace Ui {
class KnobsAndButtonsControllerMainWindow;
}

class KnobsAndButtonsControllerMainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit KnobsAndButtonsControllerMainWindow(
            JDKSAvdecc::NetIO &net_,
            JDKSAvdecc::ADPManager &adp_manager_,
            JDKSAvdecc::ControllerEntity &avdecc_controller_,
            QWidget *parent=0 );
    ~KnobsAndButtonsControllerMainWindow();

public slots:


protected:
    void timerEvent(QTimerEvent *event);

    void refreshKnobsAndButtonsListView();

    void refreshSelectedDevice();

    void refreshCompatibleControlPointsTreeView();

private:
    bool initializing;
    Ui::KnobsAndButtonsControllerMainWindow *ui;
    QBasicTimer timer;
    JDKSAvdecc::NetIO &net;
    JDKSAvdecc::ADPManager &adp_manager;
    JDKSAvdecc::ControllerEntity &avdecc_controller;
};

