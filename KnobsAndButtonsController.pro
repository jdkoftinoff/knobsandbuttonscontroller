#-------------------------------------------------
#
# Project created by QtCreator 2013-12-30T17:38:25
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KnobsAndButtonsController
TEMPLATE = app

DEFINES += US_ENABLE_RAW_ETHERNET=1 US_ENABLE_MALLOC=1

win32:LIBS += -lpcap
win32:DEFINES += US_ENABLE_PCAP=1

macx:LIBS += -lpcap
macx:DEFINES += US_ENABLE_PCAP=1

unix:!macx:LIBS += -lpcap
unix:!macx:DEFINES += US_ENABLE_PCAP=1

INCLUDEPATH += . ./JDKSAvdeccArduino ./microsupport/include

DEPENDPATH += $$INCLUDEPATH

SOURCES += \
        $$files(./*.cpp) \
        $$files(./JDKSAvdeccArduino/*.cpp) \
        $$files(./JDKSAvdeccArduino/*.c) \
        ./microsupport/src/us_allocator.c \
        ./microsupport/src/us_buffer.c \
        ./microsupport/src/us_getopt.c \
        ./microsupport/src/us_logger.c \
        ./microsupport/src/us_logger_stdio.c \
        ./microsupport/src/us_net.c \
        ./microsupport/src/us_packet.c \
        ./microsupport/src/us_packet_queue.c \
        ./microsupport/src/us_parse.c \
        ./microsupport/src/us_platform.c \
        ./microsupport/src/us_reactor.c \
        ./microsupport/src/us_reactor_handler_rawnet.c \
        ./microsupport/src/us_rawnet.c \
        ./microsupport/src/us_rawnet_multi.c \
        ./microsupport/src/us_print.c


HEADERS  += \
        $$files(./*.hpp) \
        $$files(JDKSAvdeccArduino/*.h) \
        $$files(JDKSAvdeccArduino/*.hpp) \
        $$files(JDKSAvdeccArduino/jdksavb/jdksavb/include/*.h) \
        $$files(JDKSAvdeccArduino/jdksavb/jdksavdecc-c/include/*.h) \
        $$files(microsupport/include/*.h) \
        $$files(microsupport/include/*.hpp)

FORMS    += KnobsAndButtonsControllerMainWindow.ui
