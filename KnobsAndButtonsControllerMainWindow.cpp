#include "KnobsAndButtonsControllerMainWindow.hpp"
#include "ui_KnobsAndButtonsControllerMainWindow.h"


KnobsAndButtonsControllerMainWindow::KnobsAndButtonsControllerMainWindow(
        JDKSAvdecc::NetIO &net_,
        JDKSAvdecc::ADPManager &adp_manager_,
        JDKSAvdecc::ControllerEntity &avdecc_controller_,
        QWidget *parent )
    : QMainWindow(parent)
    , initializing(true)
    , ui(new Ui::KnobsAndButtonsControllerMainWindow)
    , net( net_)
    , adp_manager( adp_manager_ )
    , avdecc_controller(avdecc_controller_) {

    ui->setupUi(this);
    timer.start(100,this);

    {
        refreshKnobsAndButtonsListView();
        refreshSelectedDevice();
        refreshCompatibleControlPointsTreeView();
    }
    refreshSelectedDevice();
    refreshCompatibleControlPointsTreeView();
    initializing=false;
}

KnobsAndButtonsControllerMainWindow::~KnobsAndButtonsControllerMainWindow() {
    timer.stop();
    delete ui;
}

void KnobsAndButtonsControllerMainWindow::refreshKnobsAndButtonsListView() {
    QTableWidget *t = ui->availableKnobsTableWidget;
    t->clear();
    QStringList horizLabels;
    horizLabels.append("MAC");
    horizLabels.append("Entity ID");
    horizLabels.append("Entity Name");
    t->setColumnCount(3);
    t->setHorizontalHeaderLabels( horizLabels );
    t->horizontalHeader()->setSectionResizeMode(0,QHeaderView::ResizeToContents);
    t->horizontalHeader()->setSectionResizeMode(1,QHeaderView::ResizeToContents);
    t->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
    t->setSelectionBehavior( QAbstractItemView::SelectRows );
    t->setRowCount(4);
    t->setItem(0,2,new QTableWidgetItem( "Entity Name 1") );
    t->setItem(0,1,new QTableWidgetItem( "Entity ID 1") );
    t->setItem(0,0,new QTableWidgetItem( "MAC 1") );
    t->setItem(1,2,new QTableWidgetItem( "Entity Name 2") );
    t->setItem(1,1,new QTableWidgetItem( "0x0011223344556677") );
    t->setItem(1,0,new QTableWidgetItem( "00:11:22:33:44:55") );
    t->setItem(2,2,new QTableWidgetItem( "Entity Name 3") );
    t->setItem(2,1,new QTableWidgetItem( "Entity ID 3") );
    t->setItem(2,0,new QTableWidgetItem( "MAC 3") );
    t->setItem(3,2,new QTableWidgetItem( "Entity Name 4") );
    t->setItem(3,1,new QTableWidgetItem( "Entity ID 4") );
    t->setItem(3,0,new QTableWidgetItem( "MAC 4") );
    t->setEditTriggers( QAbstractItemView::NoEditTriggers );
    t->setSelectionMode(QAbstractItemView::SingleSelection);
}

void KnobsAndButtonsControllerMainWindow::refreshSelectedDevice() {
    QList<QTableWidgetItem *> selected = ui->availableKnobsTableWidget->selectedItems();
    if( selected.size()>0 ) {
        ui->currentControlTargetBox->setEnabled(true);

    } else {
        ui->currentControlTargetBox->setEnabled(false);
    }
}

void KnobsAndButtonsControllerMainWindow::refreshCompatibleControlPointsTreeView() {

}

void KnobsAndButtonsControllerMainWindow::timerEvent(QTimerEvent *) {
    if( !initializing ) {
        jdksavdecc_timestamp_in_milliseconds ts = us_time_in_milliseconds();
        adp_manager.Tick(ts);
        avdecc_controller.Tick(ts);
        for( int packet_count=0; packet_count<1000; ++packet_count) {
            uint8_t buf[1500];
            uint16_t len=0;
            len = net.ReceiveRawNet(buf,sizeof(buf));
            if( len>0 ) {
                if( !adp_manager.ReceivedPDU(
                        ts,
                        buf,
                        len ) ) {
                    avdecc_controller.ReceivedPDU(ts,buf,len);
                }
            } else {
                break;
            }
        }
    }
}
