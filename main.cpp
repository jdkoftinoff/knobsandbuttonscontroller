#include "KnobsAndButtonsControllerMainWindow.hpp"
#include <QApplication>
#include "JDKSAvdeccRawNetIO.hpp"
#include "us_getopt.h"


us_rawnet_multi_t multi_rawnet;
bool option_help=false;
bool option_help_default=false;
bool option_dump=false;
bool option_dump_default=false;

us_malloc_allocator_t knobs_and_buttons_allocator;
us_getopt_t options;
us_getopt_option_t option[] = {
    {"dump","Dump settings to stdout", US_GETOPT_FLAG, &option_dump_default, &option_dump },
    {"help","Show help", US_GETOPT_FLAG, &option_help_default, &option_help },
    {0,0,US_GETOPT_NONE,0,0}
};


bool knobs_and_buttons_controller_init(const char **argv) {
    us_platform_init_sockets();
    us_malloc_allocator_init(&knobs_and_buttons_allocator);
    us_getopt_init(&options, &knobs_and_buttons_allocator.m_base);
    us_getopt_add_list(&options, option, 0, "KnobsAndButtonsController options");
    us_getopt_fill_defaults(&options);
    us_getopt_parse_args(&options,argv+1);

    if( option_dump ) {
        us_print_file_t p;
        us_print_file_init(&p, stdout);
        us_getopt_dump(&options, &p.m_base, "dump" );
        return false;
    }

    if( option_help ) {
        us_print_file_t p;
        us_print_file_init(&p, stdout);
        us_getopt_dump(&options, &p.m_base, 0 );
        return false;
    }

    us_logger_stdio_start(stdout, stderr);
    us_log_set_level(US_LOG_LEVEL_WARN);

    return true;
}

void knobs_and_buttons_controller_destroy(void) {
}


int main(int argc, char const *argv[]) {
    int r=255;
    if( knobs_and_buttons_controller_init(argv) ) {
        QApplication a(argc, (char **)argv);
        JDKSAvdecc::MultiRawNetIO net;

        jdksavdecc_eui64 entity_id;
        entity_id.value[0] = net.GetMACAddress().value[0];
        entity_id.value[1] = net.GetMACAddress().value[1];
        entity_id.value[2] = net.GetMACAddress().value[2];
        entity_id.value[3] = 0xff;
        entity_id.value[4] = 0xfe;
        entity_id.value[5] = net.GetMACAddress().value[3];
        entity_id.value[6] = net.GetMACAddress().value[4];
        entity_id.value[7] = net.GetMACAddress().value[5];

        /// This AVDECC Entity Model ID is based on the J.D. Koftinoff Software, Ltd. assigned MAC-S (OUI36): 70:b3:d5:ed:c
        jdksavdecc_eui64 entity_model_id = { {0x70, 0xb3, 0xd5, 0xed, 0xc0, 0x00, 0x01, 0x00 } };

        uint32_t entity_capabilities =
                    JDKSAVDECC_ADP_ENTITY_CAPABILITY_AEM_SUPPORTED;

        uint32_t controller_capabilities =
                    JDKSAVDECC_ADP_CONTROLLER_CAPABILITY_IMPLEMENTED;

        uint16_t valid_time_in_seconds=40;

        JDKSAvdecc::ADPManager adp_manager(
                    net,
                    entity_id,
                    entity_model_id,
                    entity_capabilities,
                    controller_capabilities,
                    valid_time_in_seconds);

        JDKSAvdecc::ControllerEntity controller_entity(adp_manager);

        KnobsAndButtonsControllerMainWindow w(net,adp_manager,controller_entity);
        w.show();
        r=a.exec();
        knobs_and_buttons_controller_destroy();
    }
    return r;
}
