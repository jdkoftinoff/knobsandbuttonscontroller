#pragma once

#include "JDKSAvdeccNetIO.hpp"
#include "us_rawnet.h"
#include "us_rawnet_multi.h"

namespace JDKSAvdecc {

class RawNetIO : public NetIO {
  protected:
    us_rawnet_context_t m_rawnet_context;
    int m_sock;
    jdksavdecc_eui48 m_mac_address;

  public:
    RawNetIO(const char *ethernet_driver);
    virtual ~RawNetIO();

    virtual void Initialize();

    virtual jdksavdecc_eui48 const &GetMACAddress() const;

    virtual uint16_t ReceiveRawNet(uint8_t *data, uint16_t max_len);

    virtual bool SendRawNet(uint8_t const *data, uint16_t len, uint8_t const *data1 = 0, uint16_t len1 = 0,
                            uint8_t const *data2 = 0, uint16_t len2 = 0);

    virtual bool SendReplyRawNet(uint8_t const *data, uint16_t len, uint8_t const *data1 = 0, uint16_t len1 = 0,
                                 uint8_t const *data2 = 0, uint16_t len2 = 0);
};

class MultiRawNetIO : public NetIO {
  protected:
    us_rawnet_multi_t m_rawnet_multi;
    jdksavdecc_eui48 m_mac_address;
    int m_last_port_polled;

  public:
    MultiRawNetIO();
    virtual ~MultiRawNetIO();

    virtual void Initialize();

    virtual jdksavdecc_eui48 const &GetMACAddress() const { return m_mac_address; }

    virtual uint16_t ReceiveRawNet(uint8_t *data, uint16_t max_len);

    virtual bool SendRawNet(uint8_t const *data, uint16_t len, uint8_t const *data1 = 0, uint16_t len1 = 0,
                            uint8_t const *data2 = 0, uint16_t len2 = 0);

    virtual bool SendReplyRawNet(uint8_t const *data, uint16_t len, uint8_t const *data1 = 0, uint16_t len1 = 0,
                                 uint8_t const *data2 = 0, uint16_t len2 = 0);
};

}
