#include "JDKSAvdeccWorld.hpp"
#include "JDKSAvdeccRawNetIO.hpp"

namespace JDKSAvdecc {

RawNetIO::RawNetIO(const char *ethernet_driver) {
    m_sock = us_rawnet_socket(&m_rawnet_context, JDKSAVDECC_AVTP_ETHERTYPE, ethernet_driver, jdksavdecc_multicast_adp_acmp.value);
    if (m_sock < 0) {
        throw std::runtime_error("Unable to open raw ethernet");
    }
    m_mac_address = jdksavdecc_eui48_get(m_rawnet_context.m_my_mac, 0);
}

RawNetIO::~RawNetIO() { us_rawnet_close(&m_rawnet_context); }

void RawNetIO::Initialize() {
    // Nothing to do here.
}

jdksavdecc_eui48 const &RawNetIO::GetMACAddress() const { return m_mac_address; }

uint16_t RawNetIO::ReceiveRawNet(uint8_t *data, uint16_t max_len) {
    ssize_t r = -1;
    r = us_rawnet_recv(&m_rawnet_context, &data[JDKSAVDECC_FRAME_HEADER_SA_OFFSET], &data[JDKSAVDECC_FRAME_HEADER_DA_OFFSET],
                       &data[JDKSAVDECC_FRAME_HEADER_LEN], max_len - JDKSAVDECC_FRAME_HEADER_LEN);

    if (r > 0) {
        // returned length includes frame header
        r += JDKSAVDECC_FRAME_HEADER_LEN;
        // manually stick ethertype in there as well
        data[JDKSAVDECC_FRAME_HEADER_ETHERTYPE_OFFSET + 0] = (JDKSAVDECC_AVTP_ETHERTYPE >> 8) & 0xff;
        data[JDKSAVDECC_FRAME_HEADER_ETHERTYPE_OFFSET + 1] = (JDKSAVDECC_AVTP_ETHERTYPE >> 0) & 0xff;
    }
    return r;
}

bool RawNetIO::SendRawNet(uint8_t const *data, uint16_t len, uint8_t const *data1, uint16_t len1, uint8_t const *data2,
                          uint16_t len2) {
    bool r = false;
    uint8_t buf[1500];

    // Put pieces together into one buffer, with frame header
    memcpy(buf, data, len);
    if (data1 && len1) {
        memcpy(buf + len, data1, len1);
    }
    if (data2 && len2) {
        memcpy(buf + len + len1, data2, len2);
    }
    uint16_t total_len = len + len1 + len2;
    ssize_t sent = us_rawnet_send(&m_rawnet_context, &buf[JDKSAVDECC_FRAME_HEADER_DA_OFFSET], &buf[JDKSAVDECC_FRAME_HEADER_LEN],
                                  total_len - JDKSAVDECC_FRAME_HEADER_LEN);
    if (sent > 0) {
        r = true;
    }
    return r;
}

bool RawNetIO::SendReplyRawNet(uint8_t const *data, uint16_t len, uint8_t const *data1, uint16_t len1, uint8_t const *data2,
                               uint16_t len2) {
    bool r = false;
    uint8_t buf[1500];
    // Send reply to whoever sent me this frame
    // Put pieces together into one buffer, with frame header
    memcpy(buf, data, len);
    if (data1 && len1) {
        memcpy(buf + len, data1, len1);
    }
    if (data2 && len2) {
        memcpy(buf + len + len1, data2, len2);
    }
    uint16_t total_len = len + len1 + len2;
    ssize_t sent = us_rawnet_send(&m_rawnet_context, &buf[JDKSAVDECC_FRAME_HEADER_SA_OFFSET], &buf[JDKSAVDECC_FRAME_HEADER_LEN],
                                  total_len - JDKSAVDECC_FRAME_HEADER_LEN);
    if (sent > 0) {
        r = true;
    }
    return r;
}

MultiRawNetIO::MultiRawNetIO()
    : m_last_port_polled(0) {

    us_rawnet_multi_open(
                &m_rawnet_multi,
                JDKSAVDECC_AVTP_ETHERTYPE,
                jdksavdecc_multicast_adp_acmp.value,
                jdksavdecc_jdks_multicast_log.value);

    if( m_rawnet_multi.ethernet_port_count>0 ) {
        m_mac_address = jdksavdecc_eui48_get(m_rawnet_multi.ethernet_ports[0].m_my_mac, 0);
    } else {
        jdksavdecc_eui48_init(&m_mac_address);
    }
}

MultiRawNetIO::~MultiRawNetIO() {
    us_rawnet_multi_close(&m_rawnet_multi);
}

void MultiRawNetIO::Initialize() {
 // Nothing to do here
}

uint16_t MultiRawNetIO::ReceiveRawNet(
        uint8_t *data,
        uint16_t max_len) {
    ssize_t len=0;
    time_t cur_time = time(0);

    for( int i=0; i<m_rawnet_multi.ethernet_port_count; ++i ) {
        int port_to_poll = (m_last_port_polled + i+1) % m_rawnet_multi.ethernet_port_count;

        len = us_rawnet_recv(
                    &m_rawnet_multi.ethernet_ports[port_to_poll],
                    &data[6],
                    &data[0],
                    &data[14],
                    max_len-14 );

        if (len>0) {
            // returned length includes frame header
            len += 14;
            // manually stick ethertype in there as well
            data[12 + 0] = (m_rawnet_multi.ethernet_ports[port_to_poll].m_ethertype >> 8) & 0xff;
            data[12 + 1] = (m_rawnet_multi.ethernet_ports[port_to_poll].m_ethertype >> 0) & 0xff;
            // update the routing table based on this source mac address being seen on this interface
            us_rawnet_multi_route_update_or_add(&m_rawnet_multi, &data[6], port_to_poll, cur_time);

            m_last_port_polled = port_to_poll;
            break;
        }
    }
    // clean up any stale routes
    us_rawnet_multi_route_cleanup(&m_rawnet_multi, cur_time);
    uint16_t r=0;
    if( len>0 ) {
        r=(uint16_t)len;
    }
    return r;
}

bool MultiRawNetIO::SendRawNet(
        const uint8_t *data,
        uint16_t len,
        const uint8_t *data1,
        uint16_t len1,
        const uint8_t *data2,
        uint16_t len2) {
    return us_rawnet_multi_send_all(
                &m_rawnet_multi,
                const_cast<uint8_t *>(data), len,
                const_cast<uint8_t *>(data1), len1,
                const_cast<uint8_t *>(data2), len2 )>0;
}


bool MultiRawNetIO::SendReplyRawNet(
        uint8_t const *data,
        uint16_t len,
        uint8_t const *data1,
        uint16_t len1,
        uint8_t const *data2,
        uint16_t len2) {
    return us_rawnet_multi_send_reply_all(
                &m_rawnet_multi,
                const_cast<uint8_t *>(data), len,
                const_cast<uint8_t *>(data1), len1,
                const_cast<uint8_t *>(data2), len2 )>0;
}
}

